all: start

start:
	yacc -o 1.c 1.y
	yacc -o 2.c 2.y
	gcc 1.c -o 1
	gcc 2.c -o 2

clean:
	rm -rf 1
	rm -rf 2
	rm -rf *.c